#!/usr/local/bin/python3.4
# --------------------------------------------
# Author: rasergiy@gmail.com
# https://bitbucket.org/rasergiy/zabbix_hpmsa-p2000


import os
import re
import sys
import json
import time
import logging
import hashlib
import requests
import optparse
import tempfile
import subprocess
import zabbix_api
import xml.etree.ElementTree


log_file      = "/var/log/zabbix/hp_p2000.log"
conf_dir      = "/usr/local/etc/hp_p2000"
conf_file     = os.path.join(conf_dir, 'hp_p2000.conf')
zabbix_sender = "/usr/local/bin/zabbix_sender"
zabbix_confd  = "/usr/local/etc/zabbix24/zabbix_agentd.conf"

log  = logging.getLogger('hp_p2000')
log.setLevel(logging.DEBUG)

OBJECTMAP = {
        'controller': {
            'type': 'Controller',               # Object type
            'prefix': 'HPCNTRL_',               # Autodiscovery variable prefix
            'url': 'controllers',               # API url for get info
            'name-key': 'durable-id',           # Name parameter
            'stats': 'controller-statistics',   # API url for statistic
            'item-name': 'controllers',         # Item names 
            'values': (                         # Statistic items
                "cpu-load", "iops", "power-on-time", "number-of-reads", 
                "data-read-numeric", "bytes-per-second-numeric", "number-of-writes", 
                "data-written-numeric")},
        'vdisk': {
            'type': 'Vdisk',
            'prefix': 'HPVDISK_',
            'url': 'vdisks',
            'name-key': 'name',
            'stats': 'vdisk-statistics',
            'item-name': 'virtual-disk',
            'values': ('array-drive-type-numeric', 'blocks', 'diskcount', 
                'health-numeric', 'iops', 'number-of-reads', 'data-read-numeric', 
                'size-numeric', 'bytes-per-second-numeric', 'number-of-writes', 
                'data-written-numeric')},
        'volume': {
            'type': 'Volume',
            'prefix': 'HPVOLUME_',
            'url': 'volumes',
            'name-key': 'volume-name',
            'stats': 'volume-statistics',
            'item-name': 'volume' ,
            'values': ('blocks', 'iops', 'number-of-reads', 'data-read-numeric', 
                'size-numeric', 'bytes-per-second-numeric', 'number-of-writes', 
                'data-written-numeric')}}

def confimp(var, config):
    ''' -> variable from config '''
    try:
        val = subprocess.check_output("/bin/sh -c '. %s; echo $%s'" % (config, var),
                shell=True, stderr=subprocess.STDOUT).decode('UTF-8').strip()
    except subprocess.CalledProcessError:
        return None
    return val


class HpMsa:

    sesskey = None

    def __init__(self, zhostname, api_host, username, password, protocol):
        '''
        zhostname - zabbix host name ('host.host' item, see:
            https://www.zabbix.com/documentation/2.0/manual/appendix/api/host/definitions
        api_host - ip/dns name of the hp p2000 api
        username - api username
        password - api password
        protocol - api protocol
        '''
        self.api_host = api_host
        self.zhostname = zhostname
        self.protocol = protocol
        digest = hashlib.new('md5')
        digest.update(str('%s_%s' % (username, password)).encode())
        self.token = digest.hexdigest()

    def login(self):
        self.session = requests.Session()
        xml = self.fetch('api/login/%s' % self.token)
        if not xml:
            return None
        rc = xml.find(u"./OBJECT/PROPERTY[@name='return-code']").text
        self.sesskey = xml.find(u"./OBJECT/PROPERTY[@name='response']").text
        if rc=='1':
            log.debug('Logged in')
            return True
        log.warn('Return-code: %s' % rc)
        log.warn(self.__last_ret__.content)
        return None

    def fetch(self, url):
        log.debug('FETCH: %s' % url)
        if self.sesskey:
            headers = {'sessionKey': self.sesskey, 'dataType': 'api'}
        else:
            headers = {}
        try:
            ret = self.session.get(self.url(url), headers=headers)
        except requests.exceptions.ConnectionError:
            log.warn('Connection error: %s' % url)
            return None
        self.__last_ret__ = ret
        if ret.status_code != 200:
            log.warn('Status code: %s' % ret.status_code)
            log.warn(ret.content)
            return None
        return xml.etree.ElementTree.fromstring(ret.content)
    
    def fetch_objects(self, url, itemname):
        objects = list()
        try:
            xoml = hp.fetch("api/show/%s" % url)
            if xoml:
                for xobj in xoml.findall('OBJECT'):
                    if xobj.attrib['name'] == itemname:
                        obj = dict()
                        for xprop in xobj.findall('PROPERTY'):
                            obj[xprop.attrib['name']] = xprop.text
                        objects.append(obj)
        except xml.etree.ElementTree.ParseError:
            log.warn('Wrong xml received. No objects!')
        return objects
    
    def exit(self):
        try:
            self.session.get(self.url('api/exit'))
        except requests.exceptions.ConnectionError:
            log.warn('Api exit error')
            return None

    def get_objects(self, name):
        map = OBJECTMAP[name]
        return self.fetch_objects(map['url'], map['item-name'])
    
    def get_lld(self, *names):
        data = list()
        for name in names:
            map = OBJECTMAP[name]
            for obj in  self.fetch_objects(map['url'], map['item-name']):
                tk1 = '{#%sID}' % map['prefix']
                tk2 = '{#%sTYPE}' % map['prefix']
                data.append({
                    tk1: obj[map['name-key']].lower(),
                    tk2: map['type']})
        return data
    
    def get_stats(self, *names):
        data = list()
        def val(v):
            return v.replace(' ', '_')
        for name in names:
            map = OBJECTMAP[name]
            objects = self.fetch_objects(map['url'], map['item-name'])
            for stat in self.fetch_objects(map['stats'], map['stats']):
                for statkey in stat:
                    name =  stat[map['name-key']].lower()
                    if stat[statkey] and statkey in map['values']:
                        data.append('%s hp.p2000.stats[%s,%s,%s] %s' % (
                            self.zhostname, map['type'], name, statkey, val(stat[statkey])))
                for obj in objects:
                    name =  obj[map['name-key']].lower()
                    if name == stat[map['name-key']]:
                        for objkey in obj:
                            if obj[objkey] and objkey in map['values']:
                                data.append('%s hp.p2000.stats[%s,%s,%s] %s' % (
                                    self.zhostname, map['type'], name, objkey, val(obj[objkey])))

        return data

    def url(self, addr):
        return '%s://%s/%s' %(self.protocol, self.api_host, addr)


parser = optparse.OptionParser('%prog <lld|stat|dump> <HOSTNAME> ... [options]')
parser.add_option("-p", "--pass", dest="password", 
    default=None, help='HP P2000 Password')
parser.add_option("-u", "--user", dest="username", 
    default=None, help='HP P2000 User')
parser.add_option("-P", "--zabbix-pass", dest="zpassword", 
    default=None, help='Zabbix Password')
parser.add_option("-U", "--zabbix-user", dest="zusername", 
    default=None, help='Zabbix User')
parser.add_option("-S", "--zabbix-server", dest="zserver", 
    default=None, help='Zabbix server')
parser.add_option("-a", "--hash", dest="hash", 
    default=None, help='Hash')
parser.add_option("-s", "--https-protocol", dest="https", action="store_true",
    default=False, help='Use HTTPS protocol')
parser.add_option("-v", "--verbose", dest="verbose", action="store_true",
    default=False, help='Verbose output')
options, args = parser.parse_args()

if len(args) < 1:
    parser.error()

if options.verbose:
    loglevel = logging.DEBUG
else:
    loglevel = logging.ERROR

# -----------------------------------------------------------------------------

try:
    ch = logging.StreamHandler(sys.stdout)
    fh = logging.FileHandler(log_file)
except PermissionError:
    pass
fh.setLevel(logging.DEBUG)
ch.setLevel(loglevel)
fh.setFormatter(logging.Formatter('%(levelname)s:%(asctime)s %(message)s'))
ch.setFormatter(logging.Formatter('%(message)s'))
log.addHandler(ch)
log.addHandler(fh)

function = args[0]

if function in ('lld', 'dump'):
    if len(args) < 2:
        parser.error('Not enough arguments')
    hostnames = args[1:2]
    objects  = set(args[2:])
    for obj in objects:
        if not obj in OBJECTMAP.keys():
            parser.error('Wrong object: %s' % obj)
elif function == 'stat':
    hostnames = args[1:]
else:
    parser.error('Wrong function: %s' % function)

# =============================================================================

log.debug("CALL: %s" % ' '.join(sys.argv))

if options.https:
    protocol = 'https'
else:
    protocol = 'http'
if not options.zusername:
    options.zusername = confimp('ZABBIX_USERNAME', conf_file)
if not options.zpassword:
    options.zpassword = confimp('ZABBIX_PASSWORD', conf_file)
if not options.zserver:
    options.zserver = confimp('ZABBIX_SERVER', conf_file)
if not options.zserver:
    options.zserver = confimp('ZABBIX_SERVER', conf_file)
if not hostnames:
    hostnames = re.split(' *', confimp('HOSTS', conf_file))

log.debug("ZABBIX SERVER: %s" % options.zserver)
zabbix = zabbix_api.ZabbixAPI(options.zserver)
try:
    zabbix.login(options.zusername, options.zpassword)
    device_available = True
except zabbix_api.APITimeout:
    device_available = False




gather_stats = list()

for hostname in hostnames:
    if not device_available:
        conf = os.path.join(conf_dir, hostname)
        hostname = confimp('HOSTNAME', conf)
        gather_stats.append('%s hp.p2000.stats[online] 0' % (hostname))
    else:
        # Get interfaces with given ip/dns names
        zinterfaces = zabbix.hostinterface.get(
                    {'filter': {'ip': hostname}}) + \
                zabbix.hostinterface.get(
                    {'filter': {'dns': hostname}})
        if not zinterfaces:
            log.error('No zabbix interfaces found for: %s' % hostname)
            sys.exit(-1)
        zinterface = zinterfaces[0]
        zhosts = zabbix.host.get({'hostids': (zinterface['hostid'],)})
        if not zhosts:
            log.error('No zabbix hosts found for interface: %s' % str(zinterface))
            sys.exit(-1)
        zhost = zhosts[0]
        zhostname = zhost['host']
        if ' ' in zhostname:
            zhostname = '"%s"' % zhostname
        api_host = zinterface['ip'] or zinterface['dns']
        
        conf = os.path.join(conf_dir, api_host)
        if not options.username:
            options.username = confimp('USERNAME', conf)
        if not options.password:
            options.password = confimp('PASSWORD', conf)
        
        gather_stats.append('%s hp.p2000.stats[online] 1' % zhostname)

        if not os.path.exists(conf) and not (\
                options.username and options.password):
            parser.error('No config found and no user/password specified via arguments')

        log.debug("HP P2000 API: %s, ZABBIX HOST: %s" % (api_host, zhostname))
        hp = HpMsa( zhostname = zhostname,
                    api_host  = api_host,
                    username=options.username, 
                    password=options.password, 
                    protocol=protocol)
        if hp.login():
            if function=='lld':
                objects = hp.get_lld(*objects)
                if objects:
                    print(json.dumps({'data': objects}, indent=' '))
                else:
                    print('Objects cannot be discovered')
            elif function=='dump':
                for name, map in OBJECTMAP.items():
                    print('-'*80)
                    objs = hp.fetch_objects(map['url'], map['item-name'])[0]
                    pars = hp.fetch_objects(map['stats'], map['stats'])[0]
                    print('Parameters %s' % name)
                    for par in sorted(objs):
                        print('    <%s>' % par)
                    print('Statistics %s' % name)
                    for par in sorted(pars):
                        print('    <%s>' % par)
            else:
                gather_stats += sorted(hp.get_stats(*OBJECTMAP.keys()))
            hp.exit()

if function=='stat':
    ret = 0
    if gather_stats:
        stats = '\n'.join(gather_stats)+'\n'
        f = tempfile.NamedTemporaryFile(delete=False)
        f.write(stats.encode())
        f.close()
        log.debug('Temp file: %s' % f.name)
        try:
            cmd = '%s -c %s -i %s' % (zabbix_sender, zabbix_confd, f.name)
            out = subprocess.check_output(cmd,
                    shell=True, stderr=subprocess.STDOUT).decode('UTF-8')
            log.debug('RUN Ok: %s' % cmd)
            os.remove(f.name)
        except subprocess.CalledProcessError as err:
            out = err.output.decode('UTF-8')
            if err.returncode == 2:
                ret = 2
            else:
                ret = 1
            log.warn('RUN FAILED: %s: %s' % (err.returncode, cmd))
        log.debug("OUT:\n%s\n%s%s\n" % ('-'*80, out, '='*80))
    else:
        ret = 1
        log.warn('No data passed')
    print(ret)

